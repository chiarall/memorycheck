import unittest
import array as arr

from diamond import *


class MyTestCase(unittest.TestCase):

    def test_slidingWindows(self):
        diamond = Diamond(0)
        diamond.setAdcToCurrFactor(1)
        diamond.setCurrToDoseFactor(1)

        for i in range(1, 201):
            diamond.appendADCCount(i)
            # first element is 1 last is 200

        integratedVec = diamond.getIntegratedDoseInWindow()
        self.assertEqual(len(integratedVec), 101)
        self.assertAlmostEqual(integratedVec[0], (100 * (100 + 1) / 2) * 0.00001)
        self.assertAlmostEqual(integratedVec[1], ((100 * (100 + 1) / 2) + 100) * 0.00001)
        self.assertAlmostEqual(integratedVec[2], ((100 * (100 + 1) / 2) + 200) * 0.00001)

        self.assertAlmostEqual(integratedVec[7], ((100 * (100 + 1) / 2) + 700) * 0.00001)
        self.assertAlmostEqual(integratedVec[99], ((100 * (100 + 1) / 2) + 9900) * 0.00001)
        self.assertAlmostEqual(integratedVec[100], ((100 * (100 + 1) / 2) + 10000) * 0.00001)

    def test_doseRate(self):
        diamond = Diamond(0)
        diamond.setAdcToCurrFactor(2)
        diamond.setCurrToDoseFactor(3)

        for i in range(0, 10):
            diamond.appendADCCount(1)

        # 1*2*3=6
        self.assertEqual(arr.array('d', [6.0, 6.0, 6.0, 6.0, 6.0, 6.0, 6.0, 6.0, 6.0, 6.0]), diamond.getDoseRate())

    def test_doseRateWIthPedestal(self):
        diamond = Diamond(0)
        diamond.setAdcToCurrFactor(2)
        diamond.setCurrToDoseFactor(3)
        diamond.setPedestal(1)

        for i in range(0, 10):
            diamond.appendADCCount(2)

        diamond.applyPedestal()
        # (2-1)*3*2 = 6.0
        self.assertEqual(arr.array('d', [6.0, 6.0, 6.0, 6.0, 6.0, 6.0, 6.0, 6.0, 6.0, 6.0]), diamond.getDoseRate())

    def test_slidingWindowssmall(self):
        diamond = Diamond(0)
        diamond.setAdcToCurrFactor(1)
        diamond.setCurrToDoseFactor(1)

        for i in range(0, 101):
            diamond.appendADCCount(1)
            # first element is 1 last is 200

        integratedVec = diamond.getIntegratedDoseInWindow()
        self.assertEqual(len(integratedVec), 2)


if __name__ == '__main__':
    unittest.main()
