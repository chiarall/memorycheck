import os
import unittest

from memorycore import *


class MyTestCase(unittest.TestCase):
    def test_readChannel(self):
        memoryCore = MemoryCore()
        memoryCore.readFile("../data/IP192.168.13.13_Time1552161131")

        diamond = memoryCore.getDiamond(0)
        channel = diamond.getChannelName()

        self.assertEqual(0, channel)
        self.assertEqual(100000, len(diamond.doseRateVec))

        diamond = memoryCore.getDiamond(1)
        channel = diamond.getChannelName()

        self.assertEqual(1, channel)
        self.assertEqual(100000, len(diamond.doseRateVec))

        diamond = memoryCore.getDiamond(2)
        channel = diamond.getChannelName()

        self.assertEqual(2, channel)
        self.assertEqual(100000, len(diamond.doseRateVec))

        diamond = memoryCore.getDiamond(3)
        channel = diamond.getChannelName()

        self.assertEqual(3, channel)
        self.assertEqual(100000, len(diamond.doseRateVec))

    def test_saveFilePlot(self):
        memoryCore = MemoryCore()
        memoryCore.readFile("../data/IP192.168.13.13_Time1552161131")

        memoryCore.plotAdcVsTime()
        memoryCore.plotDoserateVsTime()

        self.assertTrue(os.path.isfile('./adc_value0.root'))


    def test_savePlotInRunningWindow(self):
        memoryCore = MemoryCore()
        memoryCore.readFile("../data/IP192.168.13.13_Time1552161131")

        memoryCore.plotInRunningWindow()

        self.assertTrue(os.path.isfile('./IntegratedInRunningWindow_value0.root'))

    def test_IntegratedDoseInOneSec(self):
        memoryCore = MemoryCore()
        memoryCore.readFile("../data/IP192.168.13.13_Time1552161131")

        intDose = memoryCore.IntegratedDoseInOneSec()
        self.assertEqual(501925516.808, intDose['0'])

    def test_readXml(self):
        memoryCore = MemoryCore()
        memoryCore.readFile("../data/IP192.168.13.13_Time1559800431")
        memoryCore.parse("../data/radmon_20190605-16_49_49.xml","192.168.13.13")
        memoryCore.applyPedestalToDiamond()
        memoryCore.setUnixTime(1559800431)

        values = memoryCore.getDiamond(0).getIntegratedDoseInWindow()

        self.assertEqual(3, len(values))
        self.assertAlmostEqual(-1.15385099988384, values[0])
        self.assertAlmostEqual(-1.20102715518623, values[1])
        self.assertAlmostEqual(-1.25973435018847, values[2])


if __name__ == '__main__':
    unittest.main()
