from array import array

from ROOT import TGraph, TH1F, TCanvas

INTEGRATION_TIME_WINDOW = 100  # 1 ms
TIME_BETWEEN_CONSECUTIVE_DATA = 0.00001  # time interval between two consecutive data elements. This is because the memory are read at 100kHz and the file contains 1s of history
SAMPLING_FREQUENCY = 100000
TEN_MICROSECOND = 0.00001


class Diamond:

    def __init__(self, name):
        self.channel = name
        self.label = "DiaDefaultLabel"
        self.adcVec = array('d')
        self.timeVec = array('d')

        self.currentToDoseRate = 0
        self.adcToCurrent = 0
        self.pedestal = 0

    def setCurrToDoseFactor(self, currentToDoseRate):
        self.currentToDoseRate = float(currentToDoseRate)

    def setAdcToCurrFactor(self, adcToCurrent):
        self.adcToCurrent = float(adcToCurrent)

    def setPedestal(self, pedestal):
        self.pedestal = float(pedestal)

    def applyPedestal(self):
        tempAdcVec = array('d')
        for i in self.adcVec:
            tempAdcVec.append(i - float(self.pedestal))
        self.adcVec = tempAdcVec

    def appendADCCount(self, doseRate):
        self.adcVec.append(float(doseRate))
        self.timeVec.append(float(len(self.timeVec)) / SAMPLING_FREQUENCY + TEN_MICROSECOND) #TODO Missing on Server

    def getDoseRate(self):
        doserate = array('d')
        for i in self.adcVec:
            doserate.append(i * self.adcToCurrent * self.currentToDoseRate)
        return doserate

    def getIntegratedDoseInWindow(self):
        integratedDoseVec = array('d')
        doseRateVec = self.getDoseRate()

        for i in range(0, len(doseRateVec) - INTEGRATION_TIME_WINDOW + 1):
            vec = doseRateVec[i:i + INTEGRATION_TIME_WINDOW]
            multVec = [x * TIME_BETWEEN_CONSECUTIVE_DATA for x in vec]
            integratedDoseVec.append(sum(multVec))

        return integratedDoseVec

    def getIntegratedDoseInOneSec(self):
        integratedDoseInOneSec = 0
        doseRateVec = self.getDoseRate()

        for i in range(0, len(doseRateVec)):
            integratedDoseInOneSec = integratedDoseInOneSec + doseRateVec[i] * TIME_BETWEEN_CONSECUTIVE_DATA
        return integratedDoseInOneSec

    def get1msIntegration(self):
        histo = TH1F("histo", "histo", 100, -10, 10)
        sum = 0
        doseRateVec = self.getDoseRate()

        for i in range(0, len(doseRateVec)):
            sum = sum + doseRateVec[i] * TIME_BETWEEN_CONSECUTIVE_DATA
            if (i+1)%100 == 0:
                histo.Fill(sum)
                sum = 0
        return histo

    def get40usIntegration(self):
        histo_40us = TH1F("histo 40 us", "histo 40 us", 100, -1, 1)
        sum_40us = 0
        doseRateVec = self.getDoseRate()

        for i in range(0, len(doseRateVec)):
            sum_40us = sum_40us + doseRateVec[i] * TIME_BETWEEN_CONSECUTIVE_DATA
            if (i+1)%4 == 0:
                histo_40us.Fill(sum_40us)
                sum_40us = 0
        return histo_40us

    def get100usIntegration(self):
        histo_100us = TH1F("histo 100 us", "histo 100 us", 100, -1, 1)
        sum_100us = 0
        doseRateVec = self.getDoseRate()

        for i in range(0, len(doseRateVec)):
            sum_100us = sum_100us + doseRateVec[i] * TIME_BETWEEN_CONSECUTIVE_DATA
            if (i+1)%10 == 0:
                histo_100us.Fill(sum_100us)
                sum_100us = 0
        return histo_100us


    def getChannelName(self):
        return self.channel

    def setLabel(self, label):
        self.label = label

    def plot(self, timeStamp_double, doseRate_double, labelX, labelY):
        gr = TGraph(len(timeStamp_double), timeStamp_double, doseRate_double)
        gr.SetLineColor(2)
        gr.SetLineWidth(4)
        gr.SetName(self.label)
        gr.SetMarkerColor(4)
        gr.SetMarkerStyle(21)
        gr.SetMarkerSize(0.35)
        gr.SetTitle(str(self.channel))
        gr.GetXaxis().SetLabelOffset(0.02)
        gr.GetXaxis().SetTitleOffset(1.4)
        gr.GetYaxis().SetTitleOffset(1.4)
        gr.GetXaxis().SetTitle(labelX)
        gr.GetYaxis().SetTitle(labelY)
        return gr
