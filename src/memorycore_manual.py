import os
import re
import subprocess
import sys
import xmltodict
import time
from datetime import datetime

import ROOT as r
from ROOT import TCanvas, TGraph, TFile, TTree, TTreeReader, TMultiGraph, TPad, TGaxis, TAxis

from diamond import *


class MemoryCore:
    iptodcu = {"192.168.13.13": "DCU1", "192.168.13.14": "DCU2",
               "192.168.13.15": "DCU3", "192.168.13.16": "DCU4",
               "192.168.13.17": "DCU5", "192.168.13.18": "DCU6"}

    def __init__(self):
        self.unixTime = "0"
        self.diamondList = []

    def readFile(self, dataFile):
        print "New file detected: " + dataFile
        infile = open(dataFile, "r")
        channel = -1
        for line in infile:
            if (line.startswith("Memory Pointer was")):
                continue
            if (line.startswith("Reading Chanel") and line.find("name") != -1):
                parts = re.compile(",\s|\s").split(line)
                channel = int(parts[2])
                self.diamondList.insert(channel, Diamond(channel))
            elif (line.startswith("Reading Chanel")):
                continue
            elif (line.startswith("Conv:DR")):
                parts = re.compile("=|\s").split(line)
                currentToDoseRate = parts[1]
                self.diamondList[channel].setCurrToDoseFactor(currentToDoseRate)
            elif (line.startswith("Conv:Curr")):
                parts = re.compile("=|\s|,\s").split(line)
                adcToCurrent = parts[1]
                pedestal = parts[3]
                self.diamondList[channel].setAdcToCurrFactor(adcToCurrent)
                self.diamondList[channel].setPedestal(pedestal)
            else:
                parts = re.compile(";\s|\s").split(line)
                adcValue = parts[1]
                self.diamondList[channel].appendADCCount(adcValue)

    def getDiamond(self, channel):
        return self.diamondList[channel]

    def plotAdcVsTime(self):
        for diamond in self.diamondList:
            c9 = TCanvas('c9', "ADC values vs time. 1s history", 900, 600)
            plot = diamond.plot(diamond.timeVec, diamond.adcVec, "Time [s]", "ADC Counts")
            plot.Draw()
            plot.SaveAs("adc_value" + str(diamond.getChannelName()) + ".root")
        return 0

    def plotDoserateVsTime(self):
        humanDate = self.convertTime(float(self.unixTime))
        print humanDate

        filename = "DoseRate_value_" + str(self.unixTime) + "_" + str(humanDate) + ".root"
        fout = TFile(filename, "UPDATE")
        for channel in self.diamondList:
            dataVec = channel.getDoseRate()
            c9 = TCanvas('c9', "DoseRate values vs time. 1s history", 900, 600)
            plot = channel.plot(channel.timeVec, dataVec, "Time [s]", "Doserate [mrad/s]")
            plot.Draw()
            plot.Write()

        # t = fout.Get('tout')
        # treeReader = TTreeReader("tout", fout)
        # print("Entries"+ str(treeReader.GetEntries(False)))
        self.dataTree = []
        # if treeReader.GetEntries(False) <= 0:
        t = TTree('tout', 'tree')
        for channel in self.diamondList:
            dataVec = channel.getDoseRate()
            doseRate = r.vector('float')(len(dataVec))
            self.dataTree.append(doseRate)
            t.Branch(str(channel.label), self.dataTree[channel.getChannelName()])
            self.convertToRootVector(dataVec, doseRate)
        t.Fill()

        fout.Write()
        fout.Close()
        return 0

    def histoInt(self):
        filename = "Int_value_in_runningWindow" + str(self.unixTime) + ".root"
        fout_int = TFile(filename, "UPDATE")

        tInt = fout_int.Get('tout_int')
        treeReader = TTreeReader("tout_int", fout_int)
        print("Entries" + str(treeReader.GetEntries(False)))
        if treeReader.GetEntries(False) <= 0:
            tInt = TTree('tout', 'tree')
        # for channel in self.diamondList:
        channel = self.diamondList[3]
        dataVecInt = channel.getIntegratedDoseInWindow()
        doseRateInt = r.vector('float')(len(dataVecInt))
        tInt.Branch(str(channel.label), doseRateInt)
        doseRateInt = self.convertToRootVector(dataVecInt, doseRateInt)
        tInt.Fill()

        fout_int.Write()
        fout_int.Close()
        return 0

    def rsyncToSKEKB(self, filename):
        command = 'rsync -avz -e "ssh -i ~/.ssh/keys/vxd-b2skbgate.key" %s b2vxdrad@b2skbgate:' % filename
        p = subprocess.Popen(command, universal_newlines=True, shell=True,
                             stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        text = p.stdout.read()
        retcode = p.wait()
        print(text)

    def convertToRootVector(self, dataVec, doseRate):
        for i in range(0, len(dataVec)):
            doseRate[i] = float(dataVec[i])
        return doseRate

    def doHisto1msIntegral(self):
        for channel in self.diamondList:
            c9 = TCanvas("integral in 1ms", "integral in 1ms", 900, 600)
            histo = channel.get1msIntegration()
            histo.Draw()
            histo.SaveAs("histogram_1m_integral_" + str(channel.getChannelName()) + ".root")
        return 0

    def doHisto40usIntegral(self):
        for channel in self.diamondList:
            c = TCanvas("integral in 40 us", "integral in 40 us", 900, 600)
            histo_40us = channel.get40usIntegration()
            histo_40us.Draw()
            histo_40us.SaveAs("histogram_40us_integral_" + str(channel.getChannelName()) + ".root")
        return 0

    def doHisto100usIntegral(self):
        for channel in self.diamondList:
            c = TCanvas("integral in 100 us", "integral in 100 us", 900, 600)
            histo_100us = channel.get100usIntegration()
            histo_100us.Draw()
            histo_100us.SaveAs("histogram_100us_integral_" + str(channel.getChannelName()) + ".root")
        return 0


    def createCorrelationPlot(self):
        for channel1 in self.diamondList:
            for channel2 in self.diamondList:
                dataVec1 = channel1.getDoseRate()
                dataVec2 = channel2.getDoseRate()
                c9 = TCanvas('c9', "correlation plot", 900, 600)
                plotCorrelation = channel1.plot(dataVec1, dataVec2, "Doserate [mrad/s]", "Doserate [mrad/s]")
                plotCorrelation.Draw("AP")
                c9.SaveAs(
                    "correlation_" + str(channel1.getChannelName()) + "_" + str(channel2.getChannelName()) + ".png")
        return 0

    def plotInRunningWindow(self):
        timeVecShift = array('d')
        timeVecShift.append(0.001)
        for i in range(1, 99901):
            timeVecShift.append(timeVecShift[i - 1] + 0.00001)

        for channel in self.diamondList:
            c9 = TCanvas('c9', "Integrated dose in running window. 1s history", 900, 600)
            dataVec = channel.getIntegratedDoseInWindow()
            plot = channel.plot(timeVecShift, dataVec, "Interval of time", "Integrated dose [mrad]")
            plot.Draw()
            plot.SaveAs("IntegratedInRunningWindow_value" + str(channel.getChannelName()) + ".root")
        return 0

    def plotOverlap(self):
        timeVecShift = array('d')
        timeVecShift.append(0.001)
        for i in range(1, 99901):
            timeVecShift.append(timeVecShift[i - 1] + 0.00001)

        for channel in self.diamondList:
            c9 = TCanvas('c9', "Integrated dose in running window. 1s history", 900, 600)
            pad = TPad("pad", "", 0, 0, 1, 1)
            pad.Draw()
            pad.cd()
            mg = TMultiGraph("mg", "mg")

            dataVec = channel.getIntegratedDoseInWindow()
            intplot = channel.plot(timeVecShift, dataVec, "Interval of time", "Integrated dose [mrad]")
            intplot.SetLineColor(2)
            intplot.SetLineWidth(2)
            intplot.SetMarkerColor(2)
            intplot.GetXaxis().SetTitle("time [s]")
            intplot.GetYaxis().SetAxisColor(2)
            intplot.GetYaxis().SetLabelColor(2)
            intplot.GetYaxis().SetTitleColor(2)

            xaxis1 = intplot.GetXaxis()
            xaxis1.SetLimits(0.98, 0.99)
            intplot.Draw("AP")

            c9.cd()
            overlay = TPad("overlay", "", 0, 0, 1, 1)
            overlay.SetFillStyle(4000)
            overlay.SetFillColor(0)
            overlay.SetFrameFillStyle(4000)
            overlay.Draw()
            overlay.cd()
            dataVec = channel.getDoseRate()
            doseplot = channel.plot(channel.timeVec, dataVec, "Time [s]", "Doserate [mrad/s]")
            xaxis2 = doseplot.GetXaxis()
            xaxis2.SetLimits(0.98, 0.99)

            doseplot.GetYaxis().SetAxisColor(4)
            doseplot.GetYaxis().SetLabelColor(4)
            doseplot.GetYaxis().SetTitleColor(4)
            doseplot.GetXaxis().SetTitle("time [s]")
            yaxis = TGaxis(0.00001, 150000, 1, 150000, 0.00001, 1, 510, "-L")
            yaxis.Draw()
            yaxis.SetTitle("doseRate")
            doseplot.Draw("APY+")

            c9.SaveAs("OverlapRunningWindow_value" + str(channel.getChannelName()) + ".root")
        return 0

    def convertTime(self, tRange):
        return datetime.fromtimestamp(tRange).strftime('%Y-%m-%d_%H:%M:%S')

    def IntegratedDoseInOneSec(self):
        for channel in self.diamondList:
            print channel.getIntegratedDoseInOneSec()
        return 0

    def parse(self, xmlFile, ip):
        nameDcu = self.iptodcu[ip]
        f = open(xmlFile, "r")
        xml = f.read()
        confDict = xmltodict.parse(xml)
        env_ = confDict["Env"]
        dcuList = env_["DCUs"]
        dcu = dcuList[nameDcu]
        rangeUsed = dcu["@Range"]
        chan = "Chan%s"
        adcConv = "@C%s"
        for i in range(0, 4):
            diamond = self.getDiamond(i)
            channel = dcu[chan % (i)]
            pedestal = channel["@Ped"]
            name = channel["@Name"]
            currentToDoseRate = channel["@F"]
            adcToCurrent = channel[adcConv % (rangeUsed)]

            diamond.setPedestal(pedestal)
            diamond.setLabel(name)
            diamond.setAdcToCurrFactor(adcToCurrent)
            diamond.setCurrToDoseFactor(currentToDoseRate)

    def setUnixTime(self, time):
        self.unixTime = time
        pass

    def applyPedestalToDiamond(self):
        for channel in self.diamondList:
            channel.applyPedestal()


def main():
    dataFile = sys.argv[1] if len(sys.argv) > 1 else '.'
    memoryCore = MemoryCore()
    memoryCore.readFile(dataFile)

    if len(sys.argv) > 2:
        xmlFile = sys.argv[2]
        filename = os.path.basename(dataFile)
        ipSearch = re.search('(?:[0-9]{1,3}\.){3}[0-9]{1,3}', filename)
        if ipSearch == None:
            print 'File name with no IP. Aborting!'
            return

        ip = ipSearch.group(0)
        memoryCore.parse(xmlFile, ip)
    memoryCore.applyPedestalToDiamond()
    filename = os.path.basename(dataFile)
    unixTime = re.search('_Time([0-9]*)', filename)
    if unixTime == None:
        print 'File name with no Time. Aborting!'
        return
    timeTest = unixTime.group(1)

    memoryCore.setUnixTime(timeTest)
    memoryCore.plotDoserateVsTime()
    # memoryCore.histoInt()
    allPlots = sys.argv[3] if len(sys.argv) > 3 else "False"
    if allPlots == "True":
        memoryCore.plotAdcVsTime()
        memoryCore.plotInRunningWindow()
        memoryCore.plotOverlap()
        # memoryCore.createCorrelationPlot()
        memoryCore.IntegratedDoseInOneSec()
        # memoryCore.doHisto1msIntegral()
        # memoryCore.doHisto40usIntegral()
        # memoryCore.doHisto100usIntegral()


if __name__ == "__main__":
    main()
