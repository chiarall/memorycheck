import os
import re
import subprocess
import sys
import xmltodict
import time
from datetime import datetime
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

import ROOT as r
from ROOT import TCanvas, TGraph, TFile, TTree, TTreeReader, TMultiGraph, TPad, TGaxis, TAxis

from diamond import *


class MemoryCore:
    diamondList = []
    iptodcu = {"192.168.13.13": "DCU1", "192.168.13.14": "DCU2",
               "192.168.13.15": "DCU3", "192.168.13.16": "DCU4",
               "192.168.13.17": "DCU5", "192.168.13.18": "DCU6"}

    def __init__(self):
        self.unixTime = "0"
        self.diamondList = []

    def readFile(self, dataFile):
        print "New file detected: " + dataFile
        infile = open(dataFile, "r")
        channel = -1
        for line in infile:
            if (line.startswith("Memory Pointer was")):
                continue
            if (line.startswith("Reading Chanel") and line.find("name") != -1):
                parts = re.compile(",\s|\s").split(line)
                channel = int(parts[2])
                self.diamondList.insert(channel, Diamond(channel))
            elif (line.startswith("Reading Chanel")):
                continue
            elif (line.startswith("Conv:DR")):
                parts = re.compile("=|\s").split(line)
                currentToDoseRate = parts[1]
                self.diamondList[channel].setCurrToDoseFactor(currentToDoseRate)
            elif (line.startswith("Conv:Curr")):
                parts = re.compile("=|\s|,\s").split(line)
                adcToCurrent = parts[1]
                pedestal = parts[3]
                self.diamondList[channel].setAdcToCurrFactor(adcToCurrent)
                self.diamondList[channel].setPedestal(pedestal)
            else:
                parts = re.compile(";\s|\s").split(line)
                adcValue = parts[1]
                self.diamondList[channel].appendADCCount(adcValue)

    def getDiamond(self, channel):
        return self.diamondList[channel]

    def plotAdcVsTime(self):
        for diamond in self.diamondList:
            c9 = TCanvas('c9', "ADC values vs time. 1s history", 900, 600)
            plot = diamond.plot(diamond.timeVec, diamond.adcVec, "Time [s]", "ADC Counts")
            plot.Draw()
            plot.SaveAs("adc_value" + str(diamond.getChannelName()) + ".root")
        return 0

    def plotDoserateVsTime(self):
        humanDate = self.convertTime(float(self.unixTime))
        print humanDate

        filename = "DoseRate_value_" + str(self.unixTime) + "_" + str(humanDate) + ".root"
        fout = TFile(filename, "UPDATE")
        for channel in self.diamondList:
            dataVec = channel.getDoseRate()
            c9 = TCanvas('c9', "DoseRate values vs time. 1s history", 900, 600)
            plot = channel.plot(channel.timeVec, dataVec, "Time [s]", "Doserate [mrad/s]")
            plot.Draw()
            plot.Write()

        #t = fout.Get('tout')
        #treeReader = TTreeReader("tout", fout)
        #print("Entries"+ str(treeReader.GetEntries(False)))
        #if treeReader.GetEntries(False) <= 0:
        t = TTree('tout', 'tree')
        self.dataTree=[]
        for channel in self.diamondList:
            dataVec = channel.getDoseRate()
            doseRate = r.vector('float')(len(dataVec))
            self.dataTree.append(doseRate)
            t.Branch(str(channel.label), self.dataTree[channel.getChannelName()])
            self.convertToRootVector(dataVec, doseRate)
        t.Fill()

        fout.Write()
        fout.Close()
        self.rsyncToSKEKB(filename)
        return 0

    def rsyncToSKEKB(self, filename):
        command = 'rsync -avz -e "ssh -i ~/.ssh/keys/vxd-b2skbgate.key" %s b2vxdrad@b2skbgate:' % filename
        p = subprocess.Popen(command, universal_newlines=True, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        text = p.stdout.read()
        retcode = p.wait()
        print(text)

    def applyPedestalToDiamond(self):
        for channel in self.diamondList:
            channel.applyPedestal()

    def convertToRootVector(self, dataVec, doseRate):
        for i in range(0, len(dataVec)):
            doseRate[i] = float(dataVec[i])
        return doseRate


    def plotInRunningWindow(self):
        timeVecShift = array('d')
        timeVecShift.append(0.001)
        for i in range(1, 99901):
            timeVecShift.append(timeVecShift[i - 1] + 0.00001)

        for channel in self.diamondList:
            c9 = TCanvas('c9', "Integrated dose in running window. 1s history", 900, 600)
            dataVec = channel.getIntegratedDoseInWindow()
            plot = channel.plot(timeVecShift, dataVec, "Interval of time", "Integrated dose [mrad]")
            plot.Draw()
            plot.SaveAs("IntegratedInRunningWindow_value" + str(channel.getChannelName()) + ".root")
        return 0

    def convertTime(self, tRange):
        return datetime.fromtimestamp(tRange).strftime('%Y-%m-%d_%H-%M-%S')

    def IntegratedDoseInOneSec(self):
        for channel in self.diamondList:
            print channel.getIntegratedDoseInOneSec()
        return 0

    def parse(self, xmlFile, ip):
        nameDcu = self.iptodcu[ip]
        f = open(xmlFile, "r")
        xml = f.read()
        confDict = xmltodict.parse(xml)
        env_ = confDict["Env"]
        dcuList = env_["DCUs"]
        dcu = dcuList[nameDcu]
        rangeUsed = dcu["@Range"]
        chan = "Chan%s"
        adcConv = "@C%s"
        for i in range(0, 4):
            diamond = self.getDiamond(i)
            channel = dcu[chan % (i)]
            pedestal = channel["@Ped"]
            name = channel["@Name"]
            currentToDoseRate = channel["@F"]
            adcToCurrent = channel[adcConv % (rangeUsed)]

            diamond.setPedestal(pedestal)
            diamond.setLabel(name)
            diamond.setAdcToCurrFactor(adcToCurrent)
            diamond.setCurrToDoseFactor(currentToDoseRate)

    def setUnixTime(self, timeAbort):
        self.unixTime = timeAbort
        pass

class DefaultFileHandker(FileSystemEventHandler):
    def on_created(self, event):
        time.sleep(10)
        dataFile = event.src_path
        if ".DS_Store" in dataFile:
            return
        try:
            memoryCore = MemoryCore()
            memoryCore.readFile(dataFile)

            if len(sys.argv) > 2:
                xmlFile = sys.argv[2]
                filename = os.path.basename(dataFile)
                ipSearch = re.search('(?:[0-9]{1,3}\.){3}[0-9]{1,3}', filename)
                if ipSearch == None:
                    print 'File name with no IP. Aborting!'
                    return

                ip = ipSearch.group(0)
                memoryCore.parse(xmlFile, ip)
            memoryCore.applyPedestalToDiamond()
            filename = os.path.basename(dataFile)
            unixTime = re.search('_Time([0-9]*)', filename)
            if unixTime == None:
                print 'File name with no Time. Aborting!'
                return
            timeAbort = unixTime.group(1)

            memoryCore.setUnixTime(timeAbort)
            memoryCore.plotDoserateVsTime()
            allPlots = sys.argv[3] if len(sys.argv) > 3 else 'False'
            if allPlots == "True":
                memoryCore.plotAdcVsTime()
                memoryCore.plotInRunningWindow()
                memoryCore.IntegratedDoseInOneSec()
        except:
            print("UNABLE TO READ FILE. SKIP")


def main():
    monitoringPath = sys.argv[1] if len(sys.argv) > 1 else '.'
    event_handler = DefaultFileHandker()
    observer = Observer()
    observer.schedule(event_handler, monitoringPath, recursive=True)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
    print 'Interrupted'


if __name__ == "__main__":
    main()
